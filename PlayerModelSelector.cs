﻿using GTA;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace PlayerModelSelector
{
    public class PlayerModelSelector : Script
    {
        bool menuOpen = false, useHoldKey;
        ushort selectedItem = 0, topItem = 0;
        Keys holdKey, pressKey, selectKey, downKey, upKey;
        Color selectedItemColor, defaultItemColor;
        #region Models
        Model[] models = new Model[] { "PLAYER",
"M_Y_MULTIPLAYER",
"F_Y_MULTIPLAYER",
//"SUPERLOD",
"IG_ANNA",
"IG_ANTHONY",
"IG_BADMAN",
"IG_BERNIE_CRANE",
"IG_BLEDAR",
"IG_BRIAN",
"IG_BRUCIE",
"IG_BULGARIN",
"IG_CHARISE",
"IG_CHARLIEUC",
"IG_CLARENCE",
"IG_DARDAN",
"IG_DARKO",
"IG_DERRICK_MC",
"IG_DMITRI",
"IG_DWAYNE",
"IG_EDDIELOW",
"IG_FAUSTIN",
"IG_FRANCIS_MC",
"IG_FRENCH_TOM",
"IG_GORDON",
"IG_GRACIE",
"IG_HOSSAN",
"IG_ILYENA",
"IG_ISAAC",
"IG_IVAN",
"IG_JAY",
"IG_JASON",
"IG_JEFF",
"IG_JIMMY",
"IG_JOHNNYBIKER",
"IG_KATEMC",
"IG_KENNY",
"IG_LILJACOB",
"IG_LILJACOBW",
"IG_LUCA",
"IG_LUIS",
"IG_MALLORIE",
"IG_MAMC",
"IG_MANNY",
"IG_MARNIE",
"IG_MEL",
"IG_MICHAEL",
"IG_MICHELLE",
"IG_MICKEY",
"IG_PACKIE_MC",
"IG_PATHOS",
"IG_PETROVIC",
"IG_PHIL_BELL",
"IG_PLAYBOY_X",
"IG_RAY_BOCCINO",
"IG_RICKY",
"IG_ROMAN",
"IG_ROMANW",
"IG_SARAH",
"IG_TUNA",
"IG_VINNY_SPAZ",
"IG_VLAD",
"CS_ANDREI",
"CS_ANGIE",
"CS_BADMAN",
"CS_BLEDAR",
"CS_BULGARIN",
"CS_BULGARINHENCH",
"CS_CIA",
"CS_DARDAN",
"CS_DAVETHEMATE",
"CS_DMITRI",
"CS_EDTHEMATE",
"CS_FAUSTIN",
"CS_FRANCIS",
"CS_HOSSAN",
"CS_ILYENA",
"CS_IVAN",
"CS_JAY",
"CS_JIMMY_PEGORINO",
"CS_MEL",
"CS_MICHELLE",
"CS_MICKEY",
"CS_OFFICIAL",
"CS_RAY_BOCCINO",
"CS_SERGEI",
"CS_VLAD",
"CS_WHIPPINGGIRL",
"CS_MANNY",
"CS_ANTHONY",
"CS_ASHLEY",
"CS_ASSISTANT",
"CS_CAPTAIN",
"CS_CHARLIEUC",
"CS_DARKO",
"CS_DWAYNE",
"CS_ELI_JESTER",
"CS_ELIZABETA",
"CS_GAYTONY",
"CS_GERRYMC",
"CS_GORDON",
"CS_ISSAC",
"CS_JOHNNYTHEBIKER",
"CS_JONGRAVELLI",
"CS_JORGE",
"CS_KAT",
"CS_KILLER",
"CS_LUIS",
"CS_MAGICIAN",
"CS_MAMC",
"CS_MELODY",
"CS_MITCHCOP",
"CS_MORI",
"CS_PBXGIRL2",
"CS_PHILB",
"CS_PLAYBOYX",
"CS_PRIEST",
"CS_RICKY",
"CS_TOMMY",
"CS_TRAMP",
"CS_BRIAN",
"CS_CHARISE",
"CS_CLARENCE",
"CS_EDDIELOW",
"CS_GRACIE",
"CS_JEFF",
"CS_MARNIE",
"CS_MARSHAL",
"CS_PATHOS",
"CS_SARAH",
"CS_ROMAN_D",
"CS_ROMAN_T",
"CS_ROMAN_W",
"CS_BRUCIE_B",
"CS_BRUCIE_T",
"CS_BRUCIE_W",
"CS_BERNIE_CRANEC",
"CS_BERNIE_CRANET",
"CS_BERNIE_CRANEW",
"CS_LILJACOB_B",
"CS_LILJACOB_J",
"CS_MALLORIE_D",
"CS_MALLORIE_J",
"CS_MALLORIE_W",
"CS_DERRICKMC_B",
"CS_DERRICKMC_D",
"CS_MICHAEL_B",
"CS_MICHAEL_D",
"CS_PACKIEMC_B",
"CS_PACKIEMC_D",
"CS_KATEMC_D",
"CS_KATEMC_W",
"M_Y_GAFR_LO_01",
"M_Y_GAFR_LO_02",
"M_Y_GAFR_HI_01",
"M_Y_GAFR_HI_02",
"M_Y_GALB_LO_01",
"M_Y_GALB_LO_02",
"M_Y_GALB_LO_03",
"M_Y_GALB_LO_04",
"M_M_GBIK_LO_03",
"M_Y_GBIK_HI_01",
"M_Y_GBIK_HI_02",
"M_Y_GBIK02_LO_02",
"M_Y_GBIK_LO_01",
"M_Y_GBIK_LO_02",
"M_Y_GIRI_LO_01",
"M_Y_GIRI_LO_02",
"M_Y_GIRI_LO_03",
"M_M_GJAM_HI_01",
"M_M_GJAM_HI_02",
"M_M_GJAM_HI_03",
"M_Y_GJAM_LO_01",
"M_Y_GJAM_LO_02",
"M_Y_GKOR_LO_01",
"M_Y_GKOR_LO_02",
"M_Y_GLAT_LO_01",
"M_Y_GLAT_LO_02",
"M_Y_GLAT_HI_01",
"M_Y_GLAT_HI_02",
"M_Y_GMAF_HI_01",
"M_Y_GMAF_HI_02",
"M_Y_GMAF_LO_01",
"M_Y_GMAF_LO_02",
"M_O_GRUS_HI_01",
"M_Y_GRUS_LO_01",
"M_Y_GRUS_LO_02",
"M_Y_GRUS_HI_02",
"M_M_GRU2_HI_01",
"M_M_GRU2_HI_02",
"M_M_GRU2_LO_02",
"M_Y_GRU2_LO_01",
"M_M_GTRI_HI_01",
"M_M_GTRI_HI_02",
"M_Y_GTRI_LO_01",
"M_Y_GTRI_LO_02",
"F_O_MAID_01",
"F_O_BINCO",
"F_Y_BANK_01",
"F_Y_DOCTOR_01",
"F_Y_GYMGAL_01",
"F_Y_FF_BURGER_R",
"F_Y_FF_CLUCK_R",
"F_Y_FF_RSCAFE",
"F_Y_FF_TWCAFE",
"F_Y_FF_WSPIZZA_R",
"F_Y_HOOKER_01",
"F_Y_HOOKER_03",
"F_Y_NURSE",
"F_Y_STRIPPERC01",
"F_Y_STRIPPERC02",
"F_Y_WAITRESS_01",
"M_M_ALCOHOLIC",
"M_M_ARMOURED",
"M_M_BUSDRIVER",
"M_M_CHINATOWN_01",
"M_M_CRACKHEAD",
"M_M_DOC_SCRUBS_01",
"M_M_DOCTOR_01",
"M_M_DODGYDOC",
"M_M_EECOOK",
"M_M_ENFORCER",
"M_M_FACTORY_01",
"M_M_FATCOP_01",
"M_M_FBI",
"M_M_FEDCO",
"M_M_FIRECHIEF",
"M_M_GUNNUT_01",
"M_M_HELIPILOT_01",
"M_M_HPORTER_01",
"M_M_KOREACOOK_01",
"M_M_LAWYER_01",
"M_M_LAWYER_02",
"M_M_LOONYBLACK",
"M_M_PILOT",
"M_M_PINDUS_01",
"M_M_POSTAL_01",
"M_M_SAXPLAYER_01",
"M_M_SECURITYMAN",
"M_M_SELLER_01",
"M_M_SHORTORDER",
"M_M_STREETFOOD_01",
"M_M_SWEEPER",
"M_M_TAXIDRIVER",
"M_M_TELEPHONE",
"M_M_TENNIS",
"M_M_TRAIN_01",
"M_M_TRAMPBLACK",
"M_M_TRUCKER_01",
"M_O_JANITOR",
"M_O_HOTEL_FOOT",
"M_O_MPMOBBOSS",
"M_Y_AIRWORKER",
"M_Y_BARMAN_01",
"M_Y_BOUNCER_01",
"M_Y_BOUNCER_02",
"M_Y_BOWL_01",
"M_Y_BOWL_02",
"M_Y_CHINVEND_01",
"M_Y_CLUBFIT",
"M_Y_CONSTRUCT_01",
"M_Y_CONSTRUCT_02",
"M_Y_CONSTRUCT_03",
"M_Y_COP",
"M_Y_COP_TRAFFIC",
"M_Y_COURIER",
"M_Y_COWBOY_01",
"M_Y_DEALER",
"M_Y_DRUG_01",
"M_Y_FF_BURGER_R",
"M_Y_FF_CLUCK_R",
"M_Y_FF_RSCAFE",
"M_Y_FF_TWCAFE",
"M_Y_FF_WSPIZZA_R",
"M_Y_FIREMAN",
"M_Y_GARBAGE",
"M_Y_GOON_01",
"M_Y_GYMGUY_01",
"M_Y_MECHANIC_02",
"M_Y_MODO",
"M_Y_NHELIPILOT",
"M_Y_PERSEUS",
"M_Y_PINDUS_01",
"M_Y_PINDUS_02",
"M_Y_PINDUS_03",
"M_Y_PMEDIC",
"M_Y_PRISON",
"M_Y_PRISONAOM",
"M_Y_ROMANCAB",
"M_Y_RUNNER",
"M_Y_SHOPASST_01",
"M_Y_STROOPER",
"M_Y_SWAT",
"M_Y_SWORDSWALLOW",
"M_Y_THIEF",
"M_Y_VALET",
"M_Y_VENDOR",
"M_Y_FRENCHTOM",
"M_Y_JIM_FITZ",
"F_O_PEASTEURO_01",
"F_O_PEASTEURO_02",
"F_O_PHARBRON_01",
"F_O_PJERSEY_01",
"F_O_PORIENT_01",
"F_O_RICH_01",
"F_M_BUSINESS_01",
"F_M_BUSINESS_02",
"F_M_CHINATOWN",
"F_M_PBUSINESS",
"F_M_PEASTEURO_01",
"F_M_PHARBRON_01",
"F_M_PJERSEY_01",
"F_M_PJERSEY_02",
"F_M_PLATIN_01",
"F_M_PLATIN_02",
"F_M_PMANHAT_01",
"F_M_PMANHAT_02",
"F_M_PORIENT_01",
"F_M_PRICH_01",
"F_Y_BUSINESS_01",
"F_Y_CDRESS_01",
"F_Y_PBRONX_01",
"F_Y_PCOOL_01",
"F_Y_PCOOL_02",
"F_Y_PEASTEURO_01",
"F_Y_PHARBRON_01",
"F_Y_PHARLEM_01",
"F_Y_PJERSEY_02",
"F_Y_PLATIN_01",
"F_Y_PLATIN_02",
"F_Y_PLATIN_03",
"F_Y_PMANHAT_01",
"F_Y_PMANHAT_02",
"F_Y_PMANHAT_03",
"F_Y_PORIENT_01",
"F_Y_PQUEENS_01",
"F_Y_PRICH_01",
"F_Y_PVILLBO_02",
"F_Y_SHOP_03",
"F_Y_SHOP_04",
"F_Y_SHOPPER_05",
"F_Y_SOCIALITE",
"F_Y_STREET_02",
"F_Y_STREET_05",
"F_Y_STREET_09",
"F_Y_STREET_12",
"F_Y_STREET_30",
"F_Y_STREET_34",
"F_Y_TOURIST_01",
"F_Y_VILLBO_01",
"M_M_BUSINESS_02",
"M_M_BUSINESS_03",
"M_M_EE_HEAVY_01",
"M_M_EE_HEAVY_02",
"M_M_FATMOB_01",
"M_M_GAYMID",
"M_M_GENBUM_01",
"M_M_LOONYWHITE",
"M_M_MIDTOWN_01",
"M_M_PBUSINESS_01",
"M_M_PEASTEURO_01",
"M_M_PHARBRON_01",
"M_M_PINDUS_02",
"M_M_PITALIAN_01",
"M_M_PITALIAN_02",
"M_M_PLATIN_01",
"M_M_PLATIN_02",
"M_M_PLATIN_03",
"M_M_PMANHAT_01",
"M_M_PMANHAT_02",
"M_M_PORIENT_01",
"M_M_PRICH_01",
"M_O_EASTEURO_01",
"M_O_HASID_01",
"M_O_MOBSTER",
"M_O_PEASTEURO_02",
"M_O_PHARBRON_01",
"M_O_PJERSEY_01",
"M_O_STREET_01",
"M_O_SUITED",
"M_Y_BOHO_01",
"M_Y_BOHOGUY_01",
"M_Y_BRONX_01",
"M_Y_BUSINESS_01",
"M_Y_BUSINESS_02",
"M_Y_CHINATOWN_03",
"M_Y_CHOPSHOP_01",
"M_Y_CHOPSHOP_02",
"M_Y_DODGY_01",
"M_Y_DORK_02",
"M_Y_DOWNTOWN_01",
"M_Y_DOWNTOWN_02",
"M_Y_DOWNTOWN_03",
"M_Y_GAYYOUNG",
"M_Y_GENSTREET_11",
"M_Y_GENSTREET_16",
"M_Y_GENSTREET_20",
"M_Y_GENSTREET_34",
"M_Y_HARDMAN_01",
"M_Y_HARLEM_01",
"M_Y_HARLEM_02",
"M_Y_HARLEM_04",
"M_Y_HASID_01",
"M_Y_LEASTSIDE_01",
"M_Y_PBRONX_01",
"M_Y_PCOOL_01",
"M_Y_PCOOL_02",
"M_Y_PEASTEURO_01",
"M_Y_PHARBRON_01",
"M_Y_PHARLEM_01",
"M_Y_PJERSEY_01",
"M_Y_PLATIN_01",
"M_Y_PLATIN_02",
"M_Y_PLATIN_03",
"M_Y_PMANHAT_01",
"M_Y_PMANHAT_02",
"M_Y_PORIENT_01",
"M_Y_PQUEENS_01",
"M_Y_PRICH_01",
"M_Y_PVILLBO_01",
"M_Y_PVILLBO_02",
"M_Y_PVILLBO_03",
"M_Y_QUEENSBRIDGE",
"M_Y_SHADY_02",
"M_Y_SKATEBIKE_01",
"M_Y_SOHO_01",
"M_Y_STREET_01",
"M_Y_STREET_03",
"M_Y_STREET_04",
"M_Y_STREETBLK_02",
"M_Y_STREETBLK_03",
"M_Y_STREETPUNK_02",
"M_Y_STREETPUNK_04",
"M_Y_STREETPUNK_05",
"M_Y_TOUGH_05",
"M_Y_TOURIST_02"
    };
        #endregion

        public PlayerModelSelector()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("selected_item_color", "colors", Color.Blue.Name);
                Settings.SetValue("default_item_color", "colors", Color.White.Name);
                Settings.SetValue("menu_toggle_hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("menu_toggle_press_key", "keys", Keys.F5);
                Settings.SetValue("item_down", "keys", Keys.NumPad2);
                Settings.SetValue("item_up", "keys", Keys.NumPad8);
                Settings.SetValue("select_item", "keys", Keys.NumPad5);
                Settings.SetValue("use_hold_key", "keys", false);
                Settings.Save();
            }

            selectedItemColor = Color.FromName(Settings.GetValueString("selected_item_color", "colors", Color.Blue.Name));
            defaultItemColor = Color.FromName(Settings.GetValueString("default_item_color", "colors", Color.White.Name));
            holdKey = Settings.GetValueKey("menu_toggle_hold_key", "keys", Keys.RControlKey);
            pressKey = Settings.GetValueKey("menu_toggle_press_key", "keys", Keys.F5);
            downKey = Settings.GetValueKey("item_down", "keys", Keys.NumPad2);
            upKey = Settings.GetValueKey("item_up", "keys", Keys.NumPad8);
            selectKey = Settings.GetValueKey("select_item", "keys", Keys.NumPad5);

            useHoldKey = Settings.GetValueBool("use_hold_key", "keys", false);

            PerFrameDrawing += PlayerModelSelector_PerFrameDrawing;
            KeyDown += PlayerModelSelector_KeyDown;
        }

        private void PlayerModelSelector_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (menuOpen)
            {
                ushort y = 0;
                for (ushort u = (ushort)topItem; ((u <= topItem + 30) && (u < models.Length)); u++)
                {
                    e.Graphics.DrawText(models[u].ToString(), 20, (y += 20), ((u == selectedItem) ? selectedItemColor : defaultItemColor));
                }
            }
        }

        private void PlayerModelSelector_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(holdKey)) && e.Key == pressKey)
            {
                Subtitle("Model selector menu is now " + ((menuOpen = !menuOpen) ? "open" : "closed"));
                selectedItem = 0;
                topItem = 0;
            }
            else if (menuOpen && e.Key == upKey)
            {
                if (selectedItem > 0)
                {
                    if (selectedItem == topItem && topItem > 0) topItem--; 
                    selectedItem--;
                }
            }
            else if (menuOpen && e.Key == selectKey)
            {
                string whatToSay;
                if (Player.Model != models[selectedItem])
                {
                    Player.Model = models[selectedItem];
                    whatToSay = "Set model to " + models[selectedItem];
                }
                else
                {
                    SetCharRandomComponentVariation(Player.Character);
                    whatToSay = "Randomized clothes of the model";
                }

                Subtitle(whatToSay);
            }
            else if (menuOpen && e.Key == downKey)
            {
                if (selectedItem < models.Length - 1)
                {
                    if (selectedItem == topItem + 30 && topItem < models.Length - 1) topItem++;
                    selectedItem++;
                }
            }
        }

        private void Subtitle(string message, int time = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", message, time, 1);
        }

        private void SetCharRandomComponentVariation(Ped ped)
        {
            GTA.Native.Function.Call("SET_CHAR_RANDOM_COMPONENT_VARIATION", ped);
        }
    }
}
