What is this mod?
Just a simple mod that changes your character's model.
	
Why make this when Simple Trainer has it already?
It was requested.
	
How to install the mod?
First install .NET Scripthook, then copy the PlayerModelSelector.net.dll and PlayerModelSelector.ini files to the scripts folder in your GTA IV directory.

How to use the mod?
Press F5 to open the menu, numpad 8 to scroll up in the menu, numpad 2 to scroll down, and numpad 5 to change your model to the selected item.

What can you change in the ini/settings file?
The keys to use the menu, and the colors of the menu.
	
What I want to add in a next version:
Scrolling down the menu when you hold the button to scroll up or down.
	
Credits:
Jackryder2014: for requesting the mod
LetsPlayOrDy (now named Jitnaught): for writing the script
	
How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
